import { combineReducers } from 'redux';
import { staticAuctions, liveAuctions } from './Auctions';

const rootReducer = combineReducers({
  staticAuctions,
  liveAuctions
});

export default rootReducer;