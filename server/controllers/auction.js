'use strict';
let views = require('co-views');
let React = require('react');
let bids = require('../../data/items');

let render = views(__dirname + '../../../views', {
  map: { html: 'jade' }
});


module.exports.home = function *home() {
  this.body = yield render('home.jade', { 'bids': bids.items });
};

function doSomeAsync() {
  return function (callback) {
    setTimeout(function () {
      callback(null, 'this was loaded asynchronously and it took 3 seconds to complete');
    }, 3000);
  };
}

// One way to deal with asynchronous call
module.exports.delay = function *delay() {
  this.body = yield doSomeAsync();
};