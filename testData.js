var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('database.db');
var fs = require("fs"); 
var faker = require('faker');

function closeDb() {
    console.log("closeDb");
    db.close();
}

 
db.serialize(function() {
  
  db.run("BEGIN TRANSACTION");
   
    // do bids
    // for (var i = 0; i < 10; i++) {

    //   var ruuid = faker.random.uuid();  
    //   var randomEmail = faker.internet.email();  
    //   var randomNumber = faker.random.number(); 

    //   db.run("INSERT INTO bids (id, item_id, amount, user_id) VALUES (?,?,?,?)", i, ruuid, randomNumber, i);
    // } 

    // do auctions
    // id, lotnumber
    for (var i = 0; i < 10; i++) {
   
      var randomNumber = faker.random.number(); 

      db.run("INSERT INTO auctions (id, amount, user_id) VALUES (?,?,?)", i, randomNumber);
    } 

    // do items
    // id, name, description, start_price
    for (var i = 0; i < 15; i++) {
  
      var productName = faker.commerce.productName();  
      var paragraph = faker.lorem.paragraph(); 
      var price = faker.commerce.price();  

      db.run("INSERT INTO items (id, name, description, start_price) VALUES (?,?,?, ?)", i, productName, paragraph, price);
    
    } 

  db.run("END");
  
})

closeDb();


 