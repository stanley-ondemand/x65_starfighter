
import { ActionTypes } from '../actions/index';

const intialState = {
  lotNumber: 1,
  view: 'liveAuction',
  bids: []
}

export function staticAuctions(state = intialState, action) {
 
  switch (action.type) {
    case ActionTypes.PLACE_BID:
      return action.event;
    case ActionTypes.VIEW_LOT:
      return state;
    case ActionTypes.RECIEVE_BIDS:
      return Object.assign({}, state, {
      bids: action.bids
    });
    case ActionTypes.SELECTED_AUCTION:
      return state;
    default:
      return state;
  }
}

export function liveAuctions(state = intialState, action) {
 
  switch (action.type) {
    case ActionTypes.PLACE_BID:
      return action.event;
    case ActionTypes.VIEW_LOT:
      return state;
    case ActionTypes.RECIEVE_BIDS:
      return state;
    case ActionTypes.SELECTED_AUCTION:
      return state;
    default:
      return state;
  }
}