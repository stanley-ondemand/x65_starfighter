
/**
 * Trying out react.js using es6 and jsx
 * 
 */
'use strict';

import io from 'socket.io-client';
import { connect } from 'react-redux';
import React from 'react'; 
import Router from 'react-router';
import { Route, RouteHandler, Link } from 'react-router';
import { Styles, Toolbar, ToolbarGroup, DropDownIcon, DropDownMenu, FontIcon, ToolbarSeparator, 
        RaisedButton, ToolbarTitle, Snackbar, IconButton, AppBar } from 'material-ui';
import LiveAuction from '../components/LiveAuction';
import StandardAuction from '../components/StandardAuction';
import Timer from '../components/Timer';

let socket = io('http://localhost:3005');
let ThemeManager = new Styles.ThemeManager();

ThemeManager.setTheme(ThemeManager.types.LIGHT)
ThemeManager.setComponentThemes({
    raisedButton: {
      primaryColor: '#992930' 
    }
})

//require('react/addons')
let filterOptions = [
  { payload: '1', text: 'Standard Auction' },
  { payload: '2', text: 'Live Auction' }
]

let iconMenuItems = [
  { payload: '1', text: 'Download' },
  { payload: '2', text: 'More Info' }
]

class App extends React.Component {

  constructor(props){
    super(props)

    this.state = {
        autoHideDuration: props.autoHideDuration
      }

    this._bidtypeChanged = this._bidtypeChanged.bind(this);
  }
 
  render () {

    const { staticAuctions, liveAuctions, dispatch } = this.props;

    let appbarStyle = {
      backgroundColor: 'rgb(226,226,226)'
    },
    auctionView;


    if(staticAuctions.view){
      auctionView =  <LiveAuction staticAuctions={liveAuctions}/>
        
    }else{
      auctionView = <StandardAuction staticAuctions={staticAuctions}/>
    }

    return (
      <div>
        <AppBar
          title="Title"
          iconClassNameRight="muidocs-icon-navigation-expand-more" style={appbarStyle}>
          <Toolbar>
            <ToolbarGroup key={0} float="left">
              <DropDownMenu menuItems={filterOptions} onChange={this._bidtypeChanged}/>
            </ToolbarGroup>
            <ToolbarGroup key={1} float="right">
              <ToolbarTitle text="Options" />
              <FontIcon className="mui-icon-sort" />
              <DropDownIcon iconClassName="icon-navigation-expand-more" menuItems={iconMenuItems} />
              <ToolbarSeparator/>
              <RaisedButton label="Place Bid Now!" primary={true} onClick={this._onClickE} />
            </ToolbarGroup>
          </Toolbar> 
        </AppBar>  
        {auctionView}
        {/*<RouteHandler/>*/}
        <Timer/>
        <IconButton iconClassName="muidocs-icon-custom-github" tooltip="GitHub"/>
        <Snackbar
          message="Your bid has been placed thankyou "
          action="undo"
          autoHideDuration={this.state.autoHideDuration}
          onActionTouchTap={this._onClickE}/> 
      </div>
    );
  }

  _bidtypeChanged() {
    const { dispatch } = this.props;
    dispatch(selectAuction('standardAuction'))
    console.log('changing my bid do stuff');
  }

  // synthetic js event
  _onClickE(event) {
    
    // todo get actully bid clicked 
    socket.emit('new bid', {
      bid: 1
    })
 
  }

  getChildContext() { 
    return {
      muiTheme: ThemeManager.getCurrentTheme()
    }
  }

}

// Important!  
App.childContextTypes = {
  muiTheme: React.PropTypes.object,
  autoHideDuration: React.PropTypes.number
}

App.propTypes = { autoHideDuration: React.PropTypes.number }
App.defaultProps = { autoHideDuration: 500 }



function mapStateToProps(state){
  const { staticAuctions, liveAuctions, dispatch } = state;

  return{
    staticAuctions,
    liveAuctions,
    dispatch
  }
}

/**
 * Could be components
 */
// class Participants extends React.Component { }
// class Bids extends React.Component { }
// class AuctionDetails extends React.Component { }
 // class Lotpicker extends React.Component { }

/**
 * Socket IO information
 */
socket.on('data', function (data) { 
  var obj = JSON.parse(data);
  App.setState({time: obj.time});
})

//when the server notifies new bid processed
socket.on('new bid', function (data) { 
   console.log(data);

   let bids = document.getElementById('biddedUser');
   let nodeClone = bids.cloneNode();

   bids.parentElement.appendChild(nodeClone);

})

export default connect(mapStateToProps)(App)

/**
 * Routes for App
 
var routes = (
  <Route handler={App}>
  </Route>
)

Router.run(routes, function (Handler) {
  React.render(<Handler/>, document.getElementById('container'));
})*/