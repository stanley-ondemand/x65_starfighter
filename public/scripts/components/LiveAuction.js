import React, { Component, PropTypes }  from 'react';  
import { Product } from '../components/Product';
import { ProductDetail } from '../components/ProductDetail';

class LiveAuction extends Component {

  render() {
    return (
      <div>
        <Product/>
        <ProductDetail/>
      </div>
      )
  }
}

export default LiveAuction;