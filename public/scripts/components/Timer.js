
import React  from 'react';  
import { Styles, Paper } from 'material-ui';

let ThemeManager = new Styles.ThemeManager();

ThemeManager.setTheme(ThemeManager.types.LIGHT);


class Timer extends React.Component { 

  render(){

    let divStyles =  {
      width: 280,
      float: 'right',
      height: 50,
      textAlign: 'center'
    },
    spanStyle = {
        fontSize: 'larger'
    };

    return (
      <div>
        <Paper style={divStyles} zDepth={1}>
          <span style={spanStyle}>Time left to Bid:</span>
          <span style={spanStyle}>7m</span> 
          <span style={spanStyle}>15s</span> 
        </Paper>
      </div>
      )
  }

  getChildContext() { 
    return {
      muiTheme: ThemeManager.getCurrentTheme()
    }
  }

}

Timer.childContextTypes = {
  muiTheme: React.PropTypes.object
}

export default Timer