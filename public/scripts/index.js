/**
 * Essentially the root of the application 
 */
import React from 'react';
import { Provider } from 'react-redux';
import App from './containers/App';
import configureStore from './store/configureStore';
import { loadBids } from './actions/index';

let injectTapEventPlugin = require("react-tap-event-plugin");

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

const store = configureStore();

// lets load the bids data upfront if no serverside rendering
store.dispatch(loadBids());

function initReactApp() {
  React.render(
    <Provider store={store}>
      {() => <App />}
    </Provider>
    , document.getElementById('container'));
}

if (window.addEventListener) {
  window.addEventListener('DOMContentLoaded', initReactApp);
}