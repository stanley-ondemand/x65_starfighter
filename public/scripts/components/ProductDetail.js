
import React, { Component, PropTypes }  from 'react';  
import { Card, Styles, CardHeader, FlatButton, CardText, 
        CardMedia, CardTitle, CardActions } from 'material-ui';

let ThemeManager = new Styles.ThemeManager();

ThemeManager.setTheme(ThemeManager.types.LIGHT);

class ProductDetail extends Component {

  /** use of ES7 class properties **/
  static contextTypes = {
    router: PropTypes.func,
    muiTheme: PropTypes.object
  }

  render () { 

    var divStyle = { paddingBottom: '5px' } 
     return (
       <Card className="ProudctDetail" style={{  marginTop: '5px'}}> 
        <CardHeader
          title="Great Britain 1840 (UNUSED) SG1"
          subtitle="Price: $281,381.90 USD"
          avatar="https://d7p5co38zcavm.cloudfront.net/large/6bb7c4cd6175979148457e4776267d7e0a8394c59856b11f2944f7d3688d5d68"/>
        <CardMedia style={{ height:'350px' }} overlay={<CardTitle title="Great Britain 1840 (UNUSED) SG1" subtitle="Price: $281,381.90 USD"/>}>
          <img className="product-detail-img"  src="https://d7p5co38zcavm.cloudfront.net/large/6bb7c4cd6175979148457e4776267d7e0a8394c59856b11f2944f7d3688d5d68"/>
        </CardMedia>
        <CardTitle title="Shipping cost to United Kingdom: $1.88" style={{ 'paddingBottom': 0}}/>
        <CardActions> 
          <FlatButton label="Item ID:  113326535"/>
          <FlatButton label="Condition: Unused"/>
        </CardActions>
        <CardText style={{ 'paddingTop': 0}}>
          <div style={divStyle}>Issue Year: 1840</div>
          <div style={divStyle}>LineEngraved</div>
          <div style={divStyle}>QueenVictoria</div>
          <div style={divStyle}>Unit weight: 0.001 kilograms</div>
          <div style={divStyle}>1840 1d Intense black Pl.5. A
            magnificent unused o.g. four margin, right hand marginal block of
            six lettered MJ-NL showing part sheet inscription. Vertical
            creasing in "J" column and horizontal crease to "M" row, with a
            small natural paper inclusion at top right of NL nevertheless a
            spectacular multiple in a lovely intense shade. A magnificent
            showpiece accompanied by a 2012 BPA Certificate.
          </div>
        </CardText>
      </Card>
    )
  }

  getChildContext() { 
      return {
        muiTheme: ThemeManager.getCurrentTheme()
    }; 
  }

}

// Important!
ProductDetail.childContextTypes = {
  muiTheme: React.PropTypes.object
}

export { ProductDetail }