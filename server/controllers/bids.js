'use strict';
let views = require('co-views');
let parse = require('co-body');
let React = require('react');
let items = require('../../data/items');

module.exports.list = function *list() {
  this.body = yield items;
};

module.exports.fetch = function *fetch(id) {
  var message = items[id];
  
  if (!message) {
    this.throw(404, 'message with id = ' + id + ' was not found');
  }
  this.body = yield message;
};

module.exports.create = function *create() {
  var message = yield parse(this);
  var id = items.push(message) - 1;
  
  message.id = id;
  this.redirect('/');
};