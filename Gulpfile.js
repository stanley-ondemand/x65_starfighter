
var gulp = require('gulp'); 
var browserify = require('browserify'); 
var babel =  require('gulp-babel');
var jade = require('gulp-jade'); 
var nodemon = require('gulp-nodemon'); 
var rename = require('gulp-rename');
var babelify = require("babelify"); 
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');  
var uglify = require('gulp-uglify');

gulp.task('css', ()=> { 
  return gulp.src('public/styles/**/*.css')
    .pipe( gulp.dest('dist/assets/styles/'))
});
 
// gulp.task('images', ()=> { 
//   return gulp.src('public/stylesheets/**/*.css')
//     .pipe( gulp.dest('dist/assets/stylesheets/'))
// });
 
// gulp.task('normal-libs', ()=> { 
//      return gulp.src('public/javascripts/lib/head.min.js')
//     .pipe( gulp.dest('dist/assets/'))
// });
 
// Browserify npm packages
//gulp.task('browserify-lib', ()=> {
//   // create a browserify bundle
//   var brwsfiy = browserify();

//   // make reveal available from outside node module lookup 
//   brwsfiy.require('reveal');

//   // start bundling
//   return brwsfiy.bundle()
//     .pipe(source('reveal.js'))   // the output file is reveal.js
//     .pipe(gulp.dest('./dist/assets'))
// })

var files = [
   './public/scripts/app.js'
];

gulp.task('js', ()=> {
  
    browserify('./public/scripts/index.js',{ debug: true })
      .transform(babelify.configure({
        stage: 0
      })) 
      // .external(require.resolve('reveal', {expose: 'Reveal'})) 
      .bundle()
      .on('error', function(err) {
        console.log('Error: ' + err.message);
      }) 
     .pipe(source('main.js'))    // the output file is main.js
     .pipe(buffer())
      //.pipe(uglify({mangle: false, compress: true }))
     .pipe(sourcemaps.init({loadMaps: true}))
     .pipe(sourcemaps.write('./'))
     .pipe(gulp.dest('./dist/assets/js')) 
});

gulp.task('templates', ()=> {
  return gulp.src('views/*.jade')
    .pipe( gulp.dest('dist/views'))
})
 
gulp.task('serve', ()=> {
  nodemon({
    script: 'server/app.js',
    //ext: 'js html',
    env: { 'NODE_ENV': 'development' }
  })
})

gulp.task('default', ['js', 'css', 'templates'])