import * as request from 'superagent';

const SELECTED_AUCTION = 'SELECT_AUCTION'
const PLACE_BID = 'PLACE_BID'
const VIEW_LOT = 'VIEW_LOT'
const RECIEVE_BIDS = 'RECIEVE_BIDS'
const ROOT = 'http://localhost:3005'

export const ActionTypes = {
  PLACE_BID,
  VIEW_LOT,
  RECIEVE_BIDS,
  SELECTED_AUCTION
}

export function placeBid() {
  return {
    type: ActionTypes.PLACE_BID
  }
}

export function selectAuction() {
  return {
    type: ActionTypes.SELECTED_AUCTION
  }
}

export function viewLot(event) {
  return {
    type: ActionTypes.VIEW_LOT,
    event
  }
}

export function recieveBids(bids) {
  return {
    type: RECIEVE_BIDS,
    bids
  }
}

export function loadBids() {
  return dispatch => {
    request.get(`${ROOT}/bids`)
      .set('Accept', 'application/json')
      .end(function (err, res) {
        debugger;
        if (res) {
          dispatch(recieveBids(res.body.items));
        }
      });
  }
};

/**
(function (dispatch) {
  request.get(ROOT + '/bids/all').set('Accept', 'application/json').end(function (err, res) {
    debugger;
    if (res) {
      dispatch(recieveBids(res.body));
    }
  });
});

**/