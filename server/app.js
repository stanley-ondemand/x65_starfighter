'use strict';
var compress = require('koa-compress');
var logger = require('koa-logger');
var serve = require('koa-static');
var route = require('koa-route');
var path = require('path');
var app = require('koa')();
var sqlite3 = require('sqlite3').verbose();
var fs = require("fs");
var db = new sqlite3.Database('database.db');

// Get controllers / routes
var auction = require('./controllers/auction');
var bids = require('./controllers/bids');

// Logger
app.use(logger());

// Routes
app.use(route.get('/', auction.home));
app.use(route.get('/bids', bids.list)); 

// Serve static files
app.use(serve('./'));

// Compress
app.use(compress());

app.use(function *(){
  this.throw('Not found', 404);
})

var server = require('http').createServer(app.callback());
var io = require('socket.io')(server);

var newBid = io
  .of('/')
  .on('connection', (socket) => { 
    setInterval( () => {
        socket.write(JSON.stringify({time: Date.now()}));
      }, 1000); 
    console.log('io');

    socket.on('new bid', () => {
      // echo to the sender
      socket.emit('new bid', `${socket.id} bid: 12`);

      // broadcast the bid to all clients
      socket.broadcast.emit('new bid', `${socket.id} bid: 15`);

    })
})

// db.serialize(function() {
//   db.run("UPDATE TABLE bids");

//   var stmt = db.prepare("INSERT INTO bids VALUES (item_id, )");
//   for (var i = 0; i < 10; i++) {
//       stmt.run({});
//   }
//   stmt.finalize();
 
// })

db.close();

server.listen(3005);
server.on('listening', () => {
  console.log('temp');
})
