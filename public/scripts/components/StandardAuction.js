import React, { Component, PropTypes }  from 'react';  
import { Product } from '../components/Product';
import { ProductDetail } from '../components/ProductDetail';

class StandardAuction extends Component {

  render() {
    return (
      <div>
        <Product/>
        <ProductDetail/>
      </div>
      )
  }
}

export default StandardAuction;