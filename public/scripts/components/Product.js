
import React, { Component, PropTypes }  from 'react';  
import { Styles, RaisedButton, List, Avatar, ListDivider, ListItem } from 'material-ui';

let ThemeManager = new Styles.ThemeManager();

ThemeManager.setTheme(ThemeManager.types.LIGHT);

class Product extends Component {

  /** use of ES7 class properties **/
  static contextTypes = {
    router: PropTypes.func,
    muiTheme: PropTypes.object
  }

  render () {

    var { router } = this.context;
    var lotId =  12;  //router.getCurrentParams().productID;
    var styles = {
        float: 'left',
        width: 300
    };

    return (
      <div className="Proudct" style={styles}>
        <h1>Lot id: {lotId}</h1>
        <List subheader="Today">
          <ListItem
            leftAvatar={<Avatar src="https://d7p5co38zcavm.cloudfront.net/large/9a37d43ffd085c4e4b90f866707c608a43f492c5f305ba2fc7e29cff3a21143f" />} 
            primaryText="Australia BCOF Japan 1946 1d brown-purple SGJ2 unused"
            secondaryText={
              <p> 
               Condition: UNUSED, Plate: 8, Issue Year: 1858, Imprimaturs, Unit weight: 0.001 kilograms
              </p>
            }
            secondaryTextLines={2} />
          <ListDivider inset={true} />
          <ListItem
            leftAvatar={<Avatar src="https://d7p5co38zcavm.cloudfront.net/large/dc8cbf89fb7d4553448c953a7371df4d57a05e8399b8540701e7b2b961976ca7" />} 
            primaryText="Kedah 1952 35c scarlet & purple SG85b unused"
            secondaryText={
              <p> 
               Condition: UNUSED, Plate: 8, Issue Year: 1858, Imprimaturs, Unit weight: 0.001 kilograms
              </p>
            }
            secondaryTextLines={2} /> 
        </List>

        <List subheader="Bids">
          <ListItem id="biddedUser"
            leftAvatar={<Avatar src="/dist/assets/avatar.png" />} 
            primaryText="User: Britain"
            secondaryTextLines={2} />
          <ListDivider inset={true} />
        </List>
        <RaisedButton label="Default" />
      </div>
    );
  }
  
  getChildContext() { 
    return {
      muiTheme: ThemeManager.getCurrentTheme()
    };
  }
 
}

// Important!
Product.childContextTypes = {
  muiTheme: React.PropTypes.object
};

export { Product  }
